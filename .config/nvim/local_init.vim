set mouse=a
noremap ; l
noremap l k
noremap k j
noremap j h
cnoreabbrev Noh noh
set showcmd
autocmd FileType markdown let g:indentLine_setConceal=0
autocmd FileType tex let g:indentLine_setConceal=0
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors
colorscheme NeoSolarized
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1
au BufRead,BufNewFile *.dat set filetype=text
au BufRead,BufNewFile *.cls set filetype=tex
au BufRead,BufNewFile *.i set filetype=cpp
au BufRead,BufNewFile *.ipp set filetype=cpp
au BufRead,BufNewFile Makefile.inc set filetype=make
autocmd FileType markdown setlocal commentstring=<!--\ %s\ -->
autocmd FileType svelte setlocal commentstring=<!--\ %s\ -->
autocmd FileType abc setlocal commentstring=%\ %s
autocmd FileType cpp setlocal commentstring=//\ %s
autocmd FileType automake setlocal commentstring=##\ %s
autocmd FileType mma setlocal commentstring=(*\ %s\ *)
let loaded_delimitMate = 1
let g:JuliaFormatter_use_sysimage=1
let g:JuliaFormatter_always_launch_server=1
nnoremap <leader>jf :JuliaFormatterFormat<CR>
nnoremap <leader>af :Autoformat<CR>
