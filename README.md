# dotfiles-nvim

DEPRECATED New config at <https://gitlab.com/eidoom/dotfiles-nvim-2>

[Companion post](https://computing-blog.netlify.app/post/neovim/)

## Description

My installation script and configuration files for `nvim`.

## Usage

### Manual installation

-   Install with 
    -   SSH clone (if you're me)
        ```shell
        git clone git@gitlab.com:eidoom/dotfiles-nvim.git
        ```
    -   or HTTPS clone (if you're not me)
        ```shell
        git clone --depth 1 https://gitlab.com/eidoom/dotfiles-nvim.git
        ```
-   then run installation script

```shell
cd dotfiles-nvim
./install.sh
```

-   `nvim` will install plugins automatically on first startup
    -   You can also manually install all plugins with `:PlugInstall` when you're in `nvim`

### Superproject

-   Alternatively, install as part of the [superproject](https://gitlab.com/eidoom/dotfiles-public).

## TODO

-   Consider: <https://github.com/amix/vimrc>
-   <https://github.com/remarkjs/remark> from <https://github.com/Chiel92/vim-autoformat>: stop from `+++` -> `\+++` on `:Au`
-   Fix Ctrl+n conflict between python-jedi & multiple-cursors
-   get indentLines to work without concealing?
-   Tweak [C++ autoformat](http://clang.llvm.org/docs/ClangFormatStyleOptions.html)

## Troubleshooting

-   If there are any problems, just run the installation script a second time.

## Updating

```shell
git pull
./install.sh
```

## Uninstall

Remove installed symlinks with
```shell
./uninstall.sh
```

## Distro support

Tested with:
* Fedora 31 
* Debian 10

## Links

* [clang-format options](https://clang.llvm.org/docs/ClangFormatStyleOptions.html)
