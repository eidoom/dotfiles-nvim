#!/usr/bin/env bash

here=$(realpath "$0" | xargs dirname)
log="$here"/install.log

while read p; do
    echo "Removing symlink $p"
    rm -r "$p"
done <"$log"

echo "Removing install log"
rm -f "$log"

