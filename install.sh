#!/usr/bin/env bash

start=$(pwd)
here=$(realpath "$0" | xargs dirname)
echo "# Moving to $here"
cd "$here" || exit
# files=(.config/nvim/local_*.vim .clang-format)
files=(.config/nvim/local_*.vim)

echo "# Installing dependancies"
if [[ $(grep PRETTY_NAME /etc/os-release | sed 's/PRETTY_NAME="\(\w\+\).*"/\1/g') == Fedora ]]; then
	sudo dnf install -y ncurses-devel git ctags-etags curl neovim python3-neovim python3-pyflakes python3-jedi python3-flake8 ripgrep clang python3-black npm golang make rustfmt
elif [[ $(grep PRETTY_NAME /etc/os-release | sed 's/PRETTY_NAME="\(\w\+\).*"/\1/g') == Debian ]]; then
	sudo apt install -y libncurses-dev git exuberant-ctags curl neovim python3-neovim python3-pyflakes python3-jedi python3-flake8 ripgrep clang black npm golang make
fi

sudo npm install -g npm neovim remark-cli js-beautify

if [ -z ${GOPATH+x} ]; then
    GOPATH=~/go
    mkdir -p "$GOPATH"
    echo "# GOPATH nonpersistently set to $GOPATH"
else
    echo "# Existing GOPATH registered at $GOPATH"
fi

echo "# Installing vim-bootstrap"
go get -u -v github.com/avelino/vim-bootstrap mvdan.cc/sh/cmd/shfmt
cd "$GOPATH/src/github.com/avelino/vim-bootstrap" || exit
go build -v
echo "# Running vim-bootstrap"
mkdir -p ~/.config/nvim
vim-bootstrap -langs=c,html,javascript,python -editor=nvim > ~/.config/nvim/init.vim

log="$here/install.log"
touch "$log"

shrc=~/.zshrc
echo "# Setting default editor in $shrc"
lines=( "EDITOR=nvim" "VISUAL=nvim" "alias vi=nvim" )
for line in "${lines[@]}"; do
    if grep -Fxq "$line" "$shrc"; then
        echo "$line already in $shrc"
    else
        echo "$line" >> "$shrc"
    fi
done

echo "# Creating symlinks for config files"
for file in "${files[@]}"; do
    if [[ "$file" == *"/"* ]]; then
        mkdir -p ~/"${file%/*}"
    fi
    link="$HOME/$file"
    ln -s "$here/$file" "$link"
    if grep -Fxq "$link" "$log"; then
        echo "$link previously installed"
    else
        echo "Logging installation of symlink $link to $log"
        echo "$link" >>"$log"
    fi
done

echo "# Moving back to $start"
cd "$start" || exit

echo "# Done!"
